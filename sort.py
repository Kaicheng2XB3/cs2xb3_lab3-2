import lab3
def insert_at(iList,iElement,iRange):
    iList.append(iList[iElement])
    if (iRange > iElement):
        for i in range(iElement + 1,iRange + 1):
            iList[i-1] = iList[i]
        iList[iRange] = iList[-1]
    elif (iRange < iElement):
        for i in range(iElement,iRange,-1):
            iList[i] = iList[i - 1]
        iList[iRange] = iList[-1]
    else:
        pass
    iList.pop(-1)
    return iList

def quicksort_inplace(L):
    if len(L) < 2:
        return L
    pivot = L[0]
    pivot_index = 0
    num = 1
    length_index = 0
    while (num < len(L)):
        if (length_index + pivot_index < len(L)):
            if L[num] < pivot:
                insert_at(L,num,0)
                pivot_index += 1
                num += 1
            else:
                insert_at(L,num,len(L))
                length_index += 1
        else:
            break
    return quicksort_inplace(L[:pivot_index]) + [pivot] + quicksort_inplace(L[pivot_index + 1:])

def dual_pivot_quicksort(L):

    # The first part: len(L) <= 2
    if len(L) < 2:
        return L
    elif len(L) == 2:
        return [min(L[0],L[1]),max(L[0],L[1])]

    # The main part.
    else:
        pivot1 = min(L[0],L[1])
        pivot2 = max(L[0],L[1])
        left, mid, right = [], [], []
        for num in L[2:]:
            if (num < pivot1):
                left.append(num)
            elif (num < pivot2):
                mid.append(num)
            else:
                right.append(num)
        return dual_pivot_quicksort(left) + [pivot1] + dual_pivot_quicksort(mid) + [pivot2] + dual_pivot_quicksort(right)
            
